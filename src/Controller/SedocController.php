<?php

namespace App\Controller;

use App\Entity\SedocHistory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SedocController extends Controller
{
    /**
     * @Route("/", name="sedoc")
     */
    public function sedocAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $result = $this->get('App\Service\WebsiteBody')->returnBodyAndRandomImageFromWebsite('https://www.sedoc.pl');

        $sedocHistory = new SedocHistory();
        $sedocHistory->setBody($result['body']);
        $em->persist($sedocHistory);
        $em->flush();

        return $this->render('sedoc/index.html.twig', [
            'randomImage' => $result['randomImage']
        ]);
    }


}