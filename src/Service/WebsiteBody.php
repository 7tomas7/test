<?php

namespace App\Service;

use Symfony\Component\DomCrawler\Crawler;

class WebsiteBody
{

    public function returnBodyAndRandomImageFromWebsite($siteUrl)
    {

        $result = array();

        $curl = curl_init($siteUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);

        $result['body'] = curl_exec($curl);

        $crawler = new Crawler($result['body']);

        $images = $crawler
            ->filter('img');
        $amountImages = $images->count();
        $position = rand(0, $amountImages - 1);

        $result['randomImage'] = $siteUrl . $crawler
                ->filter('img')->eq($position)->attr('src');

        return $result;
    }

}